from tp2 import *

#on veut pouvoir creer des boites
def test_box_creat():
    b = Box()
    
# on veut pouvoir mettre des trucs dedans
def test_box_add():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    
#tester si un truc est dans une boite
def test_box_contains():
    b = Box()
    b1 = Box()
    b1.add("truc1")
    b.add("truc1")
    b.add("truc2")
    b1.close()
    b.add("truc2")
    assert "truc1" in b
    assert "truc2" in b
    assert "truc3" not in b
    assert "truc1" in b1
    assert "truc2" not in b1
    
#test si on supprime un truc    
def test_box_remove():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.remove("truc1")
    assert "truc1" not in b
    assert "truc2" in b
    assert "truc3" not in b
    
# on veut pouvoir ouvrir ou fermer une boite
def test_is_open():
    b1 = Box()
    b2 = Box ()
    b1.open()
    b2.close()
    assert b1.is_open() == True
    assert b2.is_open() == False
    
# on veut voir le contenu de la boite
def test_action_look():
    b1 = Box()
    b2 = Box()
    b3 = Box()
    b2.close()
    b1.add("truc1")
    b1.add("truc2")
    assert b1.action_look() == "La boite contient: truc1, truc2, "  
    assert b2.action_look() == "La boite est fermée"
    assert b3.action_look() == "la boite est vide"
 
    
# on veut ajouter des choses prenant de la place
def test_chose():
    t = Thing(3)
    assert t.volume() == 3

# on veut donner une capacitée à une boite

def test_capacity():
    b = Box()
    b.set_capacity(5)
    assert b.capacity() == 5
    

#on veut savoir si il reste de la place dans une boite   
def test_has_room_for():
    t = Thing(3)
    c = Thing(6)
    b = Box()
    b.set_capacity(5)
    assert b.has_room_for(t.volume()) == True
    assert b.has_room_for(c.volume()) == False
    


#on veut mettre des chose dans une boite 
def test_box_action_add():
    b = Box()
    t = Thing(3)
    b1 = Box()
    b.set_capacity(6)
    b1.set_capacity(2)
    assert b.action_add(t) == True
    assert b1.action_add(t) == False


#on veut donner un nom a la chose
def test_chose_set_name():
    t=Thing()
    assert t.name=="bidule"
    t.set_name("chose")
    assert t.name=="chose"

#trouver l'obet de ce nom dans la boite
def test_box_find():
    t=Thing()
    b=Box()
    b.add("bidule")
    assert b.find("bidule") == "bidule"
    assert b.find("plop") == None

#si une chose a un nom
def test_chose_has_name():
    t=Thing()
    assert t.has_name("bidule")==True
    assert t.has_name("chose")==False

